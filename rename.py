# @tif 2017
import os
import re

kicad_gerber_ending = re.compile('[-][A-Z][a-z]*[.][A-Z][a-z]*.[.]gbr$')
kicad_cu = re.compile('[-][A-Z][a-z]*[.]Cu[.]gbr$')
kicad_mask = re.compile('[-][A-Z][a-z]*[.]Mask[.]gbr$')
kicad_silk = re.compile('[-][A-Z][a-z]*[.]SilkS[.]gbr$')
kicad_top = re.compile('[-]F[.][A-Z][a-z]*.[.]gbr$')
kicad_bottom = re.compile('[-]B[.][A-Z][a-z]*.[.]gbr$')
kicad_edge = re.compile('[-]Edge[.]Cuts[.]gbr$')
kicad_drill_plated = re.compile('[-]PTH[.]drl$')
kicad_drill_unplated = re.compile('[-]NPTH[.]drl$')

for file in os.listdir("."):
    if kicad_gerber_ending.search(file):
        if kicad_top.search(file):
            if kicad_cu.search(file):
                os.rename(file, kicad_gerber_ending.sub("", file) + ".GTL")
            elif kicad_mask.search(file):
                os.rename(file, kicad_gerber_ending.sub("", file) + ".GTS")
            elif kicad_silk.search(file):
                os.rename(file, kicad_gerber_ending.sub("", file) + ".GTO")
        elif kicad_bottom.search(file):
            if kicad_cu.search(file):
                os.rename(file, kicad_gerber_ending.sub("", file) + ".GBL")
            elif kicad_mask.search(file):
                os.rename(file, kicad_gerber_ending.sub("", file) + ".GBS")
            elif kicad_silk.search(file):
                os.rename(file, kicad_gerber_ending.sub("", file) + ".GBO")
        elif kicad_edge.search(file):
            os.rename(file, kicad_gerber_ending.sub("", file) + ".GML")
    elif kicad_drill_plated.search(file):
        os.rename(file, kicad_drill_plated.sub("", file) + ".TXT")
    elif kicad_drill_unplated.search(file):
        os.rename(file, kicad_drill_unplated.sub("", file) + "-NPTH.TXT")

        
